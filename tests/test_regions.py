from pprint import pprint
from typing import Any

import pydash as _

from services import regions_service


def test_fetch_regions():
    regions = regions_service.fetch_regions()

    assert _.is_list(regions) and not _.is_empty(regions)

    def every_iteratee(region: Any):
        return _.is_string(region) and not _.is_empty(region)
    assert _.every(regions, every_iteratee)

    print(f'\n** All the regions in the World **: ')
    pprint(regions)
