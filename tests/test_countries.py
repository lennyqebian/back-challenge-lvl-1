from pprint import pprint
from typing import List

import pydash as _
from pandas import DataFrame

from dtos.data_frame import DfCountry
from services import countries_service


def test_fetch_countries_by_region(regions: List[str]):
    countries = countries_service.fetch_countries_by_region(regions)

    assert isinstance(countries, DataFrame) and countries.size > 0

    def every_iteratee(country: DfCountry):
        condition = _.is_dict(country) and not _.is_empty(country) \
                    and _.has(country, 'region') \
                    and _.includes(regions, country['region'])
        return condition
    assert _.every(countries.to_dict('records'), every_iteratee)

    print(f'\n** Regiones muestra **: ')
    pprint(regions)


def test_calculate_table_process_time(countries: DataFrame):
    table_process_time = countries_service\
        .calculate_table_process_time(countries)
    time_column = countries['time']

    assert table_process_time['total_time'] == time_column.sum()
    assert table_process_time['average_time'] == time_column.mean()
    assert table_process_time['min_time'] == time_column.min()
    assert table_process_time['max_time'] == time_column.max()
