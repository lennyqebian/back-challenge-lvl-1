import random
from typing import List

import pytest
from pandas import DataFrame

from services import regions_service, countries_service


@pytest.fixture
def regions() -> List[str]:
    all_regions = regions_service.fetch_regions()
    regions = random.sample(all_regions, 3)
    return regions


@pytest.fixture
def countries(regions: List[str]) -> DataFrame:
    countries = countries_service.fetch_countries_by_region(regions)
    return countries
