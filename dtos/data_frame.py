from typing import TypedDict


class DfCountry(TypedDict):
    region: str
    name: str
    language: str
    time: float
