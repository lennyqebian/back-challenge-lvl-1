from typing import TypedDict


class Currency(TypedDict):
    code: str
    name: str
    symbol: str
