from typing import TypedDict


class Language(TypedDict):
    iso639_1: str
    iso639_2: str
    name: str
    nativeName: str
