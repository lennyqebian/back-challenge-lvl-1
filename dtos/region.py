from typing import TypedDict, List, Dict, Union

from dtos.currency import Currency
from dtos.language import Language


class RegionCountry(TypedDict):
    name: str
    topLevelDomain: List[str]
    alpha2Code: str
    alpha3Code: str
    callingCodes: List[str]
    capital: str
    altSpellings: List[str]
    region: str
    subregion: str
    population: int
    latlng: List[float]
    demonym: str
    area: float
    gini: float
    timezones: List[str]
    borders: List[str]
    nativeName: str
    numericCode: str
    currencies: Union[List[str], List[Currency]]
    languages: Union[List[str], List[Language]]
    translations: Dict[str, str]
    relevance: str
