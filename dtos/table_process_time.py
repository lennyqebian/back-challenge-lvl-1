from typing import TypedDict


class TableProcessTime(TypedDict):
    total_time: float
    average_time: float
    min_time: float
    max_time: float
