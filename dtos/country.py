from typing import List

from dtos.region import RegionCountry


class Country(RegionCountry):
    flag: str
    regionalBlocs: List[str]
    cioc: str
