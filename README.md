# Zinobe Back-end test - Challenge Python L1

## Instalación y configuración

### Requisitos del sistema

- Python v3.8 o superior

### 1. Pipenv

Asegúrese de instalar la herramienta de gestión de paquetes y entornos virtuales **Pipenv** según la documentación oficial disponible en https://pipenv.pypa.io/en/latest/#install-pipenv-today :

```bash
pip install pipenv
```

### 2. Dependencias de este proyecto

Ejecute el siguiente comando:

```bash
pipenv install
```

Para mayor información visite https://pipenv.pypa.io/en/latest/cli/#pipenv-install .

Si el entorno virtual no se inicia automáticaente, ejecute el comando:

```bash
pipenv shell
```

### 3. Autenticación con RapidAPI

Asegúrese de la existencia de la variable de entorno que corresponde a la clave de autenticación de *RapidAPI* en el sistema donde se ejecutará la aplicación:

```bash
echo ${X_RAPIDAPI_KEY}
```

Si no está configurada, asígnele el valor correspondiente ya sea con la orden `export` o con el archivo `.env`:

```bash
export X_RAPIDAPI_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

## Ejecución

### Interfaz de línea de comandos (CLI)

Para iniciar la aplicación, ejecute el siguiente comando desde el entorno virtual del proyecto:

```bash
python main.py
```

Siempre y cuando la versión por defecto de Python para el sistema sea la indicada en los requisitos anteriores, es posible ejecutar el script directamente previa configuración de permisos:

```bash
chmod +x main.py
./main.py
```
