import os

import pydash as _
from dotenv import load_dotenv


load_dotenv()

X_RAPIDAPI_HEADERS = {
    'x-rapidapi-host': os.getenv('X_RAPIDAPI_HOST',
                                 'restcountries-v1.p.rapidapi.com'),
    'x-rapidapi-key': os.getenv('X_RAPIDAPI_KEY'),
}
REGIONS_API_URL = os.getenv('REGIONS_API_URL',
                            f'https://${X_RAPIDAPI_HEADERS["x-rapidapi-host"]}')
COUNTRIES_API_URL = os.getenv('COUNTRIES_API_URL',
                              'https://restcountries.eu/rest/v2')
CACHE_EXPIRES_AFTER_SECS = _.to_number(os.getenv('CACHE_EXPIRES_AFTER_SECS',
                                                 1 * 60.0))
DB = {
    'PATH': os.getenv('DB_PATH', 'countries.db'),
}
