import json
from typing import List, Dict, Any

import requests_cache
from pandas import DataFrame
from tabulate import tabulate

import settings


def pd_tabulate(df: DataFrame):
    return tabulate(df, headers='keys', tablefmt='psql')


def init_requests_cache():
    requests_cache.install_cache('countries_reqs_cache',
                                 backend='sqlite',
                                 expire_after=settings.CACHE_EXPIRES_AFTER_SECS)


def export_2_json_file(df: DataFrame, file_path='data.json'):
    with open(file_path, 'w') as file:
        records: List[Dict[str, Any]] = df.to_dict('records')
        json.dump(records, file, indent=2)
