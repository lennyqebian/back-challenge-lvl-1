from models import Model, Attribute


class Country(Model):
    region = Attribute(type='text')
    name = Attribute(type='text', extra='PRIMARY KEY')
    language = Attribute(type='text')
    time = Attribute(type='real')
