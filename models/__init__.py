from __future__ import annotations

import inspect
import sqlite3
from abc import ABC
from dataclasses import dataclass
from typing import ClassVar, List, Union, Dict, Any, Optional, TypedDict

import pydash as _
from inflection import tableize

import settings


@dataclass
class Attribute:
    name: ClassVar[str]
    type: str
    extra: Optional[str] = None

    def __str__(self) -> str:
        field_def = f'{self.name} {self.type}'

        if not _.is_empty(self.extra):
            field_def = f'{field_def} {self.extra}'
        return field_def


class Model(ABC):
    class Meta:
        table_name: str

    connection: sqlite3.Connection

    @classmethod
    def connect(cls):
        cls.connection = sqlite3.connect(settings.DB.get('PATH'))
        print('Connection established')

    @classmethod
    def get_attributes(cls) -> List[Attribute]:
        attrs = []

        for name, attr in inspect.getmembers(cls):
            if isinstance(attr, Attribute):
                attr.name = name
                attrs.append(attr)
        return attrs

    @classmethod
    def get_attribute_names(cls) -> List[str]:
        attr_names = [
            attr.name for attr in cls.get_attributes()
        ]
        return attr_names

    @classmethod
    def create_table(cls):
        table_prop = 'table_name'

        if not _.has(cls.Meta, table_prop) \
                or _.is_empty(_.get(cls.Meta, table_prop)):
            cls.Meta.table_name = tableize(cls.__name__)
        cursor = cls.connection.cursor()
        attrs_str = ', '.join([str(attr) for attr in cls.get_attributes()])
        query = f'CREATE TABLE IF NOT EXISTS {cls.Meta.table_name}({attrs_str})'

        cursor.execute(query)
        cls.connection.commit()

    @classmethod
    def _prepare_crud(cls):
        connection_prop = 'connection'

        if _.has(cls, connection_prop) \
                and not _.is_empty(_.get(cls, connection_prop)):
            cls.connection.close()
        cls.connect()
        cls.create_table()

    @classmethod
    def create(cls, records: Union[Dict[str, Any], List[Dict[str, Any]]]):
        cls._prepare_crud()

        is_records_list = _.is_list(records)
        records = [records] if not is_records_list else records
        cursor = cls.connection.cursor()

        attr_names = cls.get_attribute_names()
        question_marks: List[str] = _.times(len(attr_names), _.constant('?'))
        question_marks_str = _.join(question_marks, ', ')
        query = f'INSERT OR IGNORE INTO {cls.Meta.table_name} ' \
                f'VALUES ({question_marks_str})'
        data = [tuple([
            record.get(attr_name) for attr_name in attr_names
        ]) for record in records]

        cursor.executemany(query, data)
        cls.connection.commit()
