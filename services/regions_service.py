from typing import List

import pandas as pd
import pydash as _
import requests

import settings
from dtos.region import RegionCountry


def fetch_regions() -> List[str]:
    url = f'{settings.REGIONS_API_URL}/all'

    res = requests.get(url, headers=settings.X_RAPIDAPI_HEADERS)

    countries: List[RegionCountry] = res.json()
    countries_df = pd.DataFrame(countries)

    by_region = countries_df.groupby('region')
    regions = _.compact(list(by_region.groups.keys()))
    return regions
