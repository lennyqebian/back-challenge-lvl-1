import random
import time
from hashlib import sha1
from typing import List, cast

import pandas as pd
import requests

import settings
from dtos.country import Country
from dtos.data_frame import DfCountry
from dtos.language import Language
from dtos.table_process_time import TableProcessTime
from utils import pd_tabulate


def _sha1_encode(target: str, encoding='utf-8') -> str:
    target_bytes = target.encode(encoding)
    res = sha1(target_bytes).hexdigest()
    return res


def fetch_country_by_region(region: str) -> DfCountry:
    starting_time = time.perf_counter()

    res = requests.get(f'{settings.COUNTRIES_API_URL}/region/{region}')

    res_countries: List[Country] = res.json()
    res_country = random.choice(res_countries)
    first_language = cast(Language, res_country['languages'][0])

    country = DfCountry(**{
        'region': region,
        'name': res_country['name'],
        'language': _sha1_encode(first_language['name']),
        'time': 0,
    })
    ending_time = time.perf_counter()
    country['time'] = (ending_time - starting_time) * 1000.0
    return country


def fetch_countries_by_region(regions: List[str]) -> pd.DataFrame:
    countries = []

    for region in regions:
        country = fetch_country_by_region(region)
        countries.append(country)
    countries_df = pd.DataFrame(countries)
    print()
    print(pd_tabulate(countries_df))

    return countries_df


def calculate_table_process_time(table: pd.DataFrame) -> TableProcessTime:
    time_column = table['time']

    table_process_time = TableProcessTime(**{
        'total_time': time_column.sum(),
        'average_time': time_column.mean(),
        'min_time': time_column.min(),
        'max_time': time_column.max(),
    })
    print()
    print(f'Total time: {table_process_time["total_time"]}ms')
    print(f'Average time: {table_process_time["average_time"]}ms')
    print(f'Min. time: {table_process_time["min_time"]}ms')
    print(f'Max. time: {table_process_time["max_time"]}ms')
    return table_process_time
