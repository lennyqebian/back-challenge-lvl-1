#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import List

from dtos.data_frame import DfCountry
from models.country import Country
from services import regions_service, countries_service
from utils import init_requests_cache, export_2_json_file


def main():
    init_requests_cache()

    regions = regions_service.fetch_regions()
    countries = countries_service.fetch_countries_by_region(regions)
    countries_service.calculate_table_process_time(countries)

    country_records: List[DfCountry] = countries.to_dict('records')
    Country.create(country_records)
    export_2_json_file(countries)


if __name__ == '__main__':
    main()
